\name{+.coordinate}
\alias{+.coordinate}
\alias{+}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Add Coordinates
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
Add a pair of coordinate objects.
}
\usage{
    first + second
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{first}{
%%     ~~Describe \code{first} here~~
first coordinate object
}
  \item{second}{
%%     ~~Describe \code{second} here~~
second coordinate object
}
}
\author{
%%  ~~who you are~~
Nick Ulle
}

%% ~Make other sections like Warning with \section{Warning }{....} ~
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (first, second) 
{
    Coordinate(first$x + second$y, first$y + second$y)
  }
}
